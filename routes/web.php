<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => '/', 'middleware' => 'auth'], function () {

  route::get('/', function () {
    return view('layouts.index');
  });

  Route::get('/titik', 'TitikController@index');
  Route::post('/titik', 'TitikController@store');
  Route::get('/titik/del/{id}', 'TitikController@destroy');

});

Route::get('/login', 'AuthController@index');
Route::post('/login', 'AuthController@store');
Route::get('/logout', 'AuthController@destroy');