# Cara Install

## Persiapan

* Install Xampp ( pastiin xampp udah jalan di browser )
* Install Composer
* Install Git
* install Node js.
* Daftar akun di Bitbucket

## Langkah install

* Pertama buat folder, kalo di windows itu di c:/xampp/htdocs/NAMA_FOLDER
* masuk ke NAMA_FOLDER
* buka Command Prompt, di Command Prompt, masuk ke folder NAMA_FOLDER
* kalo udah di Command prompt, ketik 'git init' tanpa tanda kutip
* buka link 'bitbucket.org/andreepratama27/peta'
* di pojok kanan ada kotak, ada tombol 'HTTPS', nah di kotak sebelah nya ada link 'https://andrepratama27' blablable, copy link itu
* balik ke command prompt tadi, ketik perintah 'git remote add origin paste_link_disini' tanpa tanda kutip

* tunggu

* nah kalo udah selesai, ketik composer install di command prompt
* kalo yang diatas udah selesai, ketik npm install
* kalo semua nya udah selesai, buat database dengan nama 'peta'
* kalo database udah dibuat, balik ke command prompt, ketik perintah 'php artisan migrate' dan perintah 'php artisan db:seed'

### NB : Semua perintah tanpa tanda kutip ya bos.
