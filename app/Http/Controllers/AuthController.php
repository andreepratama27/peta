<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class AuthController extends Controller
{
  public function index () {
    return view('layouts.login');
  }

  public function store (Request $r) {
    if(Auth::attempt(['email' => $r->get('email'), 'password' => $r->get('password')])) {
      return redirect('/');
    } else {
      return redirect('/login');
    }
  }

  public function destroy () {
    Auth::logout();

    return redirect('/login');
  }
}
