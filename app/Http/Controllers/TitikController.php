<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Coordinate;

class TitikController extends Controller
{
  public function index () {
    $coordinate = Coordinate::all();
    return view('layouts.titik')->with('data', $coordinate);
  }

  public function store (Request $r) {
    $coordinate = new Coordinate;
    $coordinate->name = $r->get('name');
    $coordinate->coordinate = $r->get('coordinate');

    if ($coordinate->save()) {
      return redirect('/titik');
    }
    else {
      return redirect('/titik')->with('message', 'Titik Koordinat gagal ditambahkan');
    }
  }

  public function destroy (Request $r, $id) {
    $coordinate = Coordinate::find($id)->delete();
    return redirect('/titik');
  } 
}
