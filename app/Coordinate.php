<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coordinate extends Model
{

  protected $table = 'coordinates';

  protected $fillable = [
    'name', 'coordinate'
  ];

}
