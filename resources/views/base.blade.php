<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Peta.</title>
  <link rel="stylesheet" href="{!! asset('css/app.css') !!}">
</head>
<body>
  <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
      <div class="row">
        <div class="navbar-header">
          <a href="#" class="navbar-brand">
            Peta.
          </a>
        </div>
        
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li class="active">
              <a href="/">
                Peta
              </a>
            </li>
            <li>
              <a href="/titik">
                Titik
              </a>
            </li>
            <li>
              <a href="/relasi">
                Relasi Titik
              </a>
            </li>
            <li>
              <a href="/logout">
                Logout
              </a>
            </li>
          </ul>
        </div>

      </div>
    </div>
  </nav>

  <div class="container">
    <div class="row">
    @yield('contents')
    </div>
  </div>

  @yield('script')

  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=places&language=id&key=AIzaSyCQg37pthzbzGPxPnvOodTGJZLi3s6SPCA&callback=initMap"></script>

  </body>
</html>