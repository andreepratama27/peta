@extends('base')

@section('contents')
  <div class="web-container">
    <div class="col-md-10 col-md-offset-1">
      <div class="search-location">
        <div class="search-title">
          Masukan Lokasi Pasien
        </div>
        <div class="search-input">
          <input type="text" class="form-control">
        </div>
        <div class="search-button">
          <button class="btn btn-default">Cari Rumah Sakit Terdekat</button>
        </div>
      </div>
    </div>

    <div class="col-md-12">
      <div class="map-search">
        <div class="map-box">
          <div id="map"></div>
        </div>
        <div class="map-options">
          <div class="form-group">
            <label for="" class="control-label">
              Kemacetan
            </label>
            <select name="" id="" class="form-control">
              <option value="">
                Nanti diisi
              </option>
            </select>
          </div>
          <div class="form-group">
            <label for="" class="control-label">Jalur</label>
            <p>
              RSU Bunda Thamrin Jl. Gajah Mada
            </p>
          </div>
          <div class="form-group">
            <label for="" class="control-label">Jarak Tempuh</label>
            <p>
              1.30 KM
            </p>
          </div>
          <div class="form-group">
            <label for="" class="control-label">Durasi</label>
            <p>
              1 Menit
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
<script>
  function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: 3.595196, lng: 98.672223},
      scrollwheel: false,
      zoom: 14
    }) 
  }
</script>
@endsection