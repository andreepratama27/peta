@extends('base')

@section('contents')
  <div class="web-container">
    <div class="fullscreen-maps">
      <div id="full-maps"></div>
    </div>

    <div class="form-container">
      {!! Form::open() !!}
      <div class="form-group">
        <label for="" class="control-label">Koordinat</label>
        <input type="text" class="form-control" name="coordinate">
      </div>
      <div class="form-group">
        <label for="" class="control-label">Alamat</label>
        <input type="text" class="form-control" name="name">
      </div>
      <div class="form-group">
        <button class="btn btn-default">
          Masukkan
        </button>
      </div>
      {!! Form::close() !!}
    </div>

    <div class="table-container">
      <table class="table table-bordered table-responsive">
        <thead>
          <tr>
            <th>ID</th>
            <th>Nama Titik</th>
            <th>Koordinat</th>
            <th>Operasi</th>
        </thead>
        <tbody>
          @foreach ($data as $key => $value)
          <tr>
            <td>1</td>
            <td>{{ $value->name }}</td>
            <td>{{ $value->coordinate }}</td>
            <td>
              <a href="/titik/del/{{ $value->id}}">
                Hapus
              </a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
@endsection

@section('script')
<script>
  function initMap() {
    var map = new google.maps.Map(document.getElementById('full-maps'), {
      center: {lat: -34.397, lng: 150.644},
      scrollwheel: false,
      zoom: 8
    })

    const coordinatePlace = document.querySelector('input[name=coordinate]')

    google.maps.event.addListener(map, 'click', (event) => {
      const latlng = event.latLng.lat() + ', ' + event.latLng.lng()
      coordinatePlace.value = latlng
    })
  }
</script>
@endsection