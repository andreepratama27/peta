@extends('base-login')

@section('contents')
  <div class="web-container">
    <div class="col-md-6 col-md-offset-3">
      <div class="row">
        <div class="panel panel-default panel-login">

          <div class="panel-heading">
            Login Administrator
          </div>
          
          {!! Form::open() !!}
          <div class="panel-body">
            <div class="form-group">
              <label for="" class="control-label">Email</label>
              <input type="text" class="form-control" name="email">
            </div>

            <div class="form-group">
              <label for="" class="control-label">Password</label>
              <input type="password" class="form-control" name="password">
            </div>

            <div class="form-group">
              <button class="btn btn-primary">
                Login
              </button>
            </div>
          </div>
          {!! Form::close() !!}

        </div>
      </div>
    </div>
  </div>
@endsection