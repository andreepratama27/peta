<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Peta.</title>
  <link rel="stylesheet" href="{!! asset('css/app.css') !!}">
</head>
<body>
  <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
      <div class="row">
        <div class="navbar-header">
          <a href="#" class="navbar-brand">
            Peta.
          </a>
        </div>
      </div>
    </div>
  </nav>

  <div class="container">
    <div class="row">
    @yield('contents')
    </div>
  </div>

  @yield('script')
</body>
</html>