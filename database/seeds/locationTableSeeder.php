<?php

use Illuminate\Database\Seeder;
use App\RumahSakit;

class locationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('rumahsakit')->insert([
        [
          'nama'  => 'RSUD dr. pirngadi',
          'alamat'  => 'Jl. Perintis Kemerdekaan No. 47 Medan, 20233',
          'koordinat' => '3.597894, 98.688453'
        ],
        [
          'nama'  => 'Murni Teguh Memorial Hospital',
          'alamat'  => 'Jl. Jawa No. 2 Gg. Buntu Medan Timur, 20231',
          'koordinat' => '3.590528, 98.681415'
        ],
        [
          'nama'  => 'RSU Haji Medan',
          'alamat'  => 'Jl. Rumah Sakit Haji, Medan Estate, Percut Sei Tuan, 20237',
          'koordinat' => '3.613656, 98.715060'
        ],
        [
          'nama'  => 'RS Columbia Asia Medan',
          'alamat'  => 'Jl. Listrik No. 2A Medan Petisah, 20112',
          'koordinat' => '3.585902, 98.676780'
        ],
        [
          'nama'  => 'RSU Permata Bunda',
          'alamat'  => 'Jl. Sisingamangaraja No. 7 Medan Kota, 20212',
          'koordinat' => '3.580576, 98.685818'
        ],
        [
          'nama'  => 'Rumah Sakit Medan Eye Center (Mata)',
          'alamat'  => 'Jl. Ir. H. Juanda No. 1, Medan Polonia, 20151',
          'koordinat' => '3.574496, 98.670537'
        ],
        [
          'nama'  => 'RSU Pusat H. Adam Malik (Bedah)',
          'alamat'  => 'Jl. Bunga Lau No. 17, Medan Tuntungan, 20136',
          'koordinat' => '3.518178, 98.608710'
        ],
        [
          'nama'  => 'RS Islam Malahayati (Jantung)',
          'alamat'  => 'Jl. Pangeran Diponegoro Medan Petisah, 20151',
          'koordinat' => '3.586299, 98.672735'
        ],
        [
          'nama'  => 'RS Umum Mitra Persada (Kulit)',
          'alamat'  => 'Jl. Jamin Ginting Km. 9.8 No. 166, Medan Tuntungan, 20135',
          'koordinat' => '3.526974, 98.633368'
        ],
        [
          'nama'  => 'RSU Madani (Saraf)',
          'alamat'  => 'Jl. A.R Hakim No. 168, Medan Area, 20227',
          'koordinat' => '3.575152, 98.703341'
        ]
      ]);
    }
}
